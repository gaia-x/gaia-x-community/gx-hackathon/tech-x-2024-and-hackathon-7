from pydantic import BaseModel


class CreateServiceOfferingResponse(BaseModel):
    so_vc_id: str
    so_vp_id: str
    ces_event: str
