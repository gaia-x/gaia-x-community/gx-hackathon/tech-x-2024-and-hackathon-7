from pydantic import BaseModel


class CreateLegalCredentialRequest(BaseModel):
    vat_id: str
    legal_name: str
    country_subdivision_code: str
