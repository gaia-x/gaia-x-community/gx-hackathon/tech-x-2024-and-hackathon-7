

PRIVATE_KEY_RELATIVE_PATH = "private.key"
NOTARY_HOST_URL = "https://registrationnumber.notary.lab.gaia-x.eu/v1/registrationNumberVC" #"https://gx-notary.aruba.it/v1/registrationNumberVC"
COMPLIANCE_HOST_URL = "https://compliance.lab.gaia-x.eu/v1-staging/api/credential-offers"
CES_HOST_URL = "https://ces-development.lab.gaia-x.eu/credentials-events"

POST_CREDENTIAL_HOST_URL = "https://gaiax.djustconnect.be/file?folder=participant"
POST_CREDENTIAL_API_KEY = "<YOUR KEY HERE>"

ISSUER_DID = 'did:web:gaiax.djustconnect.be'
ISSUER_KEY = "{}#JWK2020-RSA".format(ISSUER_DID)

ID_PREFIX_PARTICIPANT = "https://gaiax.djustconnect.be/participant"
