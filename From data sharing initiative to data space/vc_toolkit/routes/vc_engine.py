from fastapi import APIRouter
from models.createLegalCredentialRequest import CreateLegalCredentialRequest
from models.createLegalCredentialResponse import CreateLegalCredentialResponse
from models.createServiceOfferingRequest import CreateServiceOfferingRequest
from models.createServiceOfferingResponse import CreateServiceOfferingResponse
from services.credential_service import CredentialService
import config as config
import requests

def create_vc_engine_router() -> APIRouter:
    vc_router = APIRouter(
        prefix='/vc-engine',
        )

    @vc_router.post("/legal-participant-generator/")
    async def generate_legal_participant(request: CreateLegalCredentialRequest):
        vat_id = request.vat_id
        legal_name = request.legal_name
        country_subdivision_code = request.country_subdivision_code

        cs = CredentialService(
            config.PRIVATE_KEY_RELATIVE_PATH,
            config.ISSUER_DID,
            config.ID_PREFIX_PARTICIPANT)
        
        lrn_vc = cs.issue_registration_number(vat_id)
        print(lrn_vc)
        lp_vc = cs.issue_participant_credential(lrn_vc, legal_name, country_subdivision_code)
        print(lp_vc)
        tc_vc = cs.issue_terms_and_conditions()
        lp_vp = cs.issue_vp_legal_participant(lrn_vc, lp_vc, tc_vc)
        return CreateLegalCredentialResponse(lrn_vc_id=lrn_vc["id"], lp_vc_id=lp_vc["id"], tc_vc_id=tc_vc["id"], lp_vp_id=lp_vp["id"])

    @vc_router.post("/service-offering-generator/")
    async def generate_service_offering(request: CreateServiceOfferingRequest):

        cs = CredentialService(
            config.PRIVATE_KEY_RELATIVE_PATH,
            config.ISSUER_DID,
            config.ID_PREFIX_PARTICIPANT)
        
        lrn_vc = requests.get(request.lrn_vc_id).json()
        lp_vc = requests.get(request.lp_vc_id).json()
        tc_vc = requests.get(request.tc_vc_id).json()
        
        so_vc = cs.issue_service_offering(lp_vc["credentialSubject"]["id"], request.name, request.serviceEndpointUrl, request.termsAndConditionsUrl, request.requestType, request.formatType, request.policy)
        so_vp = cs.issue_vp_service_offering(lrn_vc, lp_vc, tc_vc, so_vc)

        ces_event = cs.push_to_credential_event_service(so_vp)

        return CreateServiceOfferingResponse(so_vc_id = so_vc["id"], so_vp_id = so_vp["id"], ces_event = ces_event)

    return vc_router