/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import axios from 'axios'
import { useEffect, useState } from 'react'

const defaultHeaders = {
  'Content-Type': 'application/json; charset=UTF-8',
}

const axiosInstance = axios.create({
  baseURL: '',
  headers: {
    ...defaultHeaders,
  },
})

const TagsPage = () => {
  const [data, setData] = useState<any>([])

  useEffect(() => {
    axiosInstance
      .get(
        'https://d3fci7hhvykw2w.cloudfront.net/get_all_resources',
        {
          headers: {},
          params: {},
        }
      )
      .then((resp: any) => {
        setData(resp.data)
      })
      .catch((error) => {
        console.log(error)
      })
  }, [])

  return (
    <div className="resource-list">
      {data.map((item: { id: string; title: string; tags: [] }, index: any) => (
        <div className="resource-item" key={index}>
          <a className="resource-title" href={item.id} target="_blank">
            {item.title}
          </a>
          <div>
            {item.tags.map((tag: any, tagIndex: any) => (
              <div className="resource-tag" key={tagIndex}>
                {tag}
              </div>
            ))}
          </div>
        </div>
      ))}
    </div>
  )
}
export default TagsPage
