import {
  STREAMLIT_PATH,
  TAGS_PATH,
  VITE_BASE_PATH,
} from '@catalogue/utility/constant'

const ROUTES_CONST = {
  ROOT: VITE_BASE_PATH,
  CATALOGUE: '',
  SO_DETAILS: 'so-details',
  STREAMLIT: STREAMLIT_PATH,
  TAGS: TAGS_PATH,
}

export { ROUTES_CONST }
