package eu.gaiax.federatedcatalogue.client;

import eu.gaiax.federatedcatalogue.client.models.LLMSearchRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

@FeignClient(value = "GaiaxLLMClient", url = "${gaiax.host.llm}")
public interface LLMClient {

    @PostMapping(value = "/search")
    Map<String, Integer> fetchIds(LLMSearchRequest prompt);

}

