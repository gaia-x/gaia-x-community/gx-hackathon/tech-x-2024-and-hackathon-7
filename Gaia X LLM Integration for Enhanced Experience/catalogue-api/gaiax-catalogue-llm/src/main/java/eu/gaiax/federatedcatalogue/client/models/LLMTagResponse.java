package eu.gaiax.federatedcatalogue.client.models;

public record LLMTagResponse(String value, long count) {
}