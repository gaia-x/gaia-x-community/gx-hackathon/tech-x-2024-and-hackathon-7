package eu.gaiax.federatedcatalogue.client.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class LLMSearchRequest {
    private String query;
}