package eu.gaiax.federatedcatalogue.client.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class LLMSearchResponse {
    private String id;
    private Integer accuracy;
}