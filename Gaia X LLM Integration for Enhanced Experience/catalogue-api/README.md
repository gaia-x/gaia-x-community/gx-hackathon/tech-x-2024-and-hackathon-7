# Gaia-x Catalogue MVP

This is MVP to showcase a simple catalogue fueled by the Credential Event Service.
It covers below user case:

1. <b>Scheduler Integration:</b> schedule calls to the Credential Exchange Service (CES) API to retrieve newly available credentials.

2. <b>Credential Verification:</b> The retrieved credentials from CES are then passed to the Signer Service for validation. If the validation is successful, the credentials are forwarded to the Policy Decision Point (PDP) endpoint to verify dynamic policies. If the verification is successful, the credentials are injected into the Neo4j database.

3. <b>Service List Retrieval:</b> Users can access a list of available services.

4. <b>Service Details Viewing and Download:</b> Users can view specific service details and download them.

5. <b>Search Functionality:</b> There are 3 types of searches available: Basic, Advanced and Smart. Each offering different criteria.

6. <b>Basic Search:</b> This search option provides common dropdown menus for selecting search criteria.

7. <b>Dynamic Search:</b> For dynamic searching, users can create grammar files using Peg.js for more flexible search capabilities.

7. <b>Smart Search:</b> It leverage machine learning over RDF2vec to provide a smart search with a best effort to reconcile query and result.


## Tools and Technologies

1. Spring boot with Neo4j 
2. Java SDK
3. Event listener
4. Neo4j 
5. jRDF2vec

## Catalogue Injection flow 
![cat-flow.png](docs/cat-flow.png)


## Run application

### Configuration

1. configure event listing 
2. configure neo4j database
3. Create k8s user with access to ingress and secret creation


### Run in IDE

1. Set values in the gaiax-catalogue-web/src/main/resources/application-{your_profile_name_}.yaml
2. Run using your favorite IDE and the newly created profile


### Run in k8s

Please refer to sample config files in ``/k8s`` folder

