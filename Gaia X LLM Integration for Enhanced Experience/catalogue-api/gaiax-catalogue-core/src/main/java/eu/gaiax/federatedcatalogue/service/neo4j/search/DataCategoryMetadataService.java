package eu.gaiax.federatedcatalogue.service.neo4j.search;

import eu.gaiax.federatedcatalogue.entity.postgres.DataCategoryMetadata;
import eu.gaiax.federatedcatalogue.model.response.TagResponse;
import eu.gaiax.federatedcatalogue.repository.postgres.DataCategoryMetadataRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class DataCategoryMetadataService {

    private final DataCategoryMetadataRepository dataCategoryMetadataRepository;

    private List<TagResponse> tags;

    public List<DataCategoryMetadata> findByCredentialSubjectId(String credentialSubjectId) {
        log.info("Getting tags by credential subject id {}", credentialSubjectId);
        return dataCategoryMetadataRepository.findByServiceId(credentialSubjectId);
    }

    public List<DataCategoryMetadata> findAll() {
        log.info("Getting mock tags");
        return dataCategoryMetadataRepository.findAll();
    }

    public List<TagResponse> findTags() {
        log.info("Getting cached tags");
        return this.tags;
    }

    @Scheduled(fixedRate = 5000)
    public void tagsLookup() {
        this.tags = dataCategoryMetadataRepository.findAll()
                .stream()
                .flatMap(item -> item.getTags().stream())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .map(entry -> new TagResponse(entry.getKey(), Math.toIntExact(entry.getValue())))
                .toList();

    }

}
