package eu.gaiax.federatedcatalogue.model.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ServiceDetailWithTagsResponse extends ServiceDetailResponse {
    private List<String> tags;
}
