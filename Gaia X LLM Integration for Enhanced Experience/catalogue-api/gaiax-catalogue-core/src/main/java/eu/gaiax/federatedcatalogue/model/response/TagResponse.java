package eu.gaiax.federatedcatalogue.model.response;


import lombok.*;

@Getter
@Builder
@NoArgsConstructor
@Setter
@AllArgsConstructor
public class TagResponse {
    private String value;
    private int count;
}
