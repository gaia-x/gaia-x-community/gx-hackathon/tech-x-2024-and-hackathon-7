package eu.gaiax.federatedcatalogue.entity.postgres;

import com.smartsensesolutions.java.commons.base.entity.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "data_category_metadata")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DataCategoryMetadata implements BaseEntity {
    @Id
    private Long id;

    @Column(name = "service_id")
    private String serviceId;

    private List<String> tags;
}
