package eu.gaiax.federatedcatalogue.repository.postgres;

import com.smartsensesolutions.java.commons.base.repository.BaseRepository;
import eu.gaiax.federatedcatalogue.entity.postgres.DataCategoryMetadata;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataCategoryMetadataRepository extends BaseRepository<DataCategoryMetadata, Long> {
        List<DataCategoryMetadata> findByServiceId(String serviceId);
}
