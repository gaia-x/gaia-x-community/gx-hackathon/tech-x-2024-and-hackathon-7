# Monorepo for Catalogue System

This monorepo contains multiple projects under different frameworks, designed to work together to provide a comprehensive catalogue system. Below is a brief overview of each project:

## Proposed Architecture
![proposed-architecture.png](.docs/proposed-architecture.png)

## Projects

### 1. catalogue-api
- **Description**: Connects with CES services and builds information on Neo4j and PostgreSQL databases for searching resources.
- **Tech Stack**: Java, Spring Boot, Neo4j, PostgreSQL
- **Key Features**:
    - Connects to CES services.
    - Builds and maintains a catalogue in Neo4j and PostgreSQL.
    - Provides an API for searching resources.
- **Details**: See [catalogue-api/README.md](catalogue-api/README.md) for more information.

### 2. catalogue-ui
- **Description**: A React application that allows users to view the catalogue and interact with a chatbot.
- **Tech Stack**: React, JavaScript, HTML, CSS
- **Key Features**:
    - User interface for browsing the catalogue.
    - Integrated chatbot for user interaction.
    - New page that loads the list of datasets with the respective tags that were populated by AI
- **Details**: See [catalogue-ui/README.md](catalogue-ui/README.md) for more information.

### 3. llm-populate-db
- **Description**: Populates PostgreSQL to be queried later by the catalogue-api and a vector database to be queried later by llm-api .
- **Tech Stack**: Python, MilvusDB, PostgreSQL
- **Key Features**:
    - Populates a vector database with 1 tag per entry (1 service with N tags will have N entries), associating it with the id URL and ServiceOfferingURL
    - Populates the PostgreSQL database with the extracted data (idURL, title and tags)
- **Details**: See [llm-populate-db/README.md](llm-populate-db/README.md) for more information.

### 4. llm-api
- **Description**: Provides search functionality. The `catalogue-ui` sends POST requests to `catalogue-api`, which gets a list of IDs from `llm-api`. For each item, the API retrieves required information from Neo4j and returns it to the `catalogue-ui`.
- **Tech Stack**: Python
- **Key Features**:
    - Processes search requests.
    - Retrieves and compiles information from Neo4j based on search IDs.
- **Details**: See [llm-api/README.md](llm-api/README.md) for more information.

### 5. chatbot-metadata
- **Description**: Provides chatbot capabilities for user interaction.
- **Tech Stack**: Python, NLP (Natural Language Processing)
- **Key Features**:
    - Chatbot functionality.
    - Interaction with users to provide information from the catalogue.
- **Details**: See [chatbot-metadata/README.md](chatbot-metadata-generator/README.md) for more information.

## Getting Started

### Installation

1. **Clone the repository**:
   ```bash
   git clone https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/tech-x-2024-and-hackathon-7.git
   cd tech-x-2024-and-hackathon-7
