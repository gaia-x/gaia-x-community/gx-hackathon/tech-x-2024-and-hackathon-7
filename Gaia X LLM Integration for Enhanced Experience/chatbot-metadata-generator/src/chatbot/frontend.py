import sys
sys.path.append('')
import streamlit as st
from src.chatbot.agent import metadataAgent
import time
from langchain_community.chat_message_histories import StreamlitChatMessageHistory
from langchain_core.runnables import RunnableConfig
from langchain.callbacks.base import BaseCallbackHandler
from langchain_core.messages.human import HumanMessage
from langchain_core.messages.ai import AIMessage

class StreamHandler(BaseCallbackHandler):
    def __init__(self, container, initial_text=""):
        self.container = container
        self.text = initial_text

    def on_llm_new_token(self, token: str, **kwargs) -> None:
        self.text += token
        self.container.markdown(self.text)

msgs = StreamlitChatMessageHistory()

st.title("Gaia-X Dataset Metadata Chatbot")

st.sidebar.title('Upload CSV')
with st.sidebar:
    with st.form("my-form", clear_on_submit=True):

        uploaded_file = st.file_uploader("Choose a CSV file", type="csv", accept_multiple_files=False)
        st.text_input("Dataset title", key="title", placeholder="Leave blank to auto-generate")
        st.text_input("Dataset description", key="description", placeholder="Leave blank to auto-generate")
        submitted = st.form_submit_button("UPLOAD!")

def handle_prompt(prompt):

    with st.chat_message("assistant"):

        stream_handler = StreamHandler(st.empty())
        cfg = RunnableConfig()
        cfg["callbacks"] = [stream_handler]
        cfg["configurable"] = {"session_id": "foo"}

        ans = st.session_state['agent'].process_conversational_rag().invoke({'input': prompt}, cfg)
        print(ans)

        msgs.add_ai_message(ans['text'])
        st.session_state['agent'].history.add_messages([HumanMessage(content=prompt), AIMessage(content=ans['text'])])
        st.rerun()

if submitted and uploaded_file is not None:
    with st.spinner(text="A fazer upload..."): 
        try:
            start = time.time()
            msgs.clear()
            st.session_state['agent'] = metadataAgent(uploaded_file)
            
        except Exception as e:
            print(e)
            st.sidebar.error(f'Error loading {uploaded_file.name}.')

    handle_prompt(f"Dataset title:{st.session_state['title']}\nDescription of the dataset:{st.session_state['description']}\nColumn names: {st.session_state['agent'].columns}\nHead: {st.session_state['agent'].sample}\n")
    print(time.time() - start)

    st.sidebar.success(f'{uploaded_file.name} uploaded successfully!')



avatars = {"human": "user", "ai": "assistant"}
for idx, msg in enumerate(msgs.messages):
    with st.chat_message(avatars[msg.type]):
        st.write(msg.content)

if prompt := st.chat_input(placeholder="Pergunta"):
    st.chat_message("user").write(prompt)
    msgs.add_user_message(prompt)
    handle_prompt(prompt)