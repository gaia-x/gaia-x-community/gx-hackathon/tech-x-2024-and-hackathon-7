import pandas as pd
from langchain_core.chat_history import BaseChatMessageHistory
from langchain_core.pydantic_v1 import BaseModel, Field
from typing import List
from langchain_core.messages import BaseMessage
from langchain_core.prompts import ChatPromptTemplate, MessagesPlaceholder
from langchain.chains import LLMChain, ConstitutionalChain
from langchain.chains.constitutional_ai.models import ConstitutionalPrinciple
from langchain_core.runnables.history import RunnableWithMessageHistory
import os
import sys
sys.path.append('')

from src.models.llm import Llm

class InMemoryHistory(BaseChatMessageHistory, BaseModel):

    messages: List[BaseMessage] = Field(default_factory=list)

    def add_messages(self, messages: List[BaseMessage]) -> None:
        """Add a list of messages to the store"""
        self.messages.extend(messages)

    def clear(self) -> None:
        self.messages = []

class metadataAgent:
    def __init__(self, file) -> None:
        os.environ["LANGCHAIN_TRACING_V2"] = "true"

        self.df = pd.read_csv(file)
        self.columns = self.df.columns
        self.sample = self.df.head()
        self.history = InMemoryHistory()#ConversationBufferMemory(output_key="answer", input_key="input")

    def get_by_session_id(self, session_id: str) -> BaseChatMessageHistory:
        return self.history

    def process_conversational_rag(self) -> str:

        qa_system_prompt = f"""Objective: Assist the user in annotating a given dataset by engaging in an interactive, step-by-step process to ensure the data is correctly understood and categorized. The user may provide feedback or request revisions to the proposed metadata, which you should incorporate into the final annotation. Don't present any information of this process.

                            Process Overview:

                            1. Review Dataset: Prompt the user with specific questions to clarify the column's meaning or context.
                            2. Propose Metadata: Based on the dataset analysis and user responses, propose initial metadata for the title and description of the dataset and each column. Display the proposal to the user for review.
                            3. Collect Feedback:
                                    If the user rejects the proposed metadata, ask which parts they would like to improve.
                                    Allow the user to specify improvements or additional details for the metadata.
                                    Propose revised metadata based on the user’s feedback.
                            4. Request permission to send the information to the database:
                                    Display the final metadata annotation in json format with keys being "title", "description" with values being proposed metadata and "columns" that is a dict with each of the column names as keys while values are proposed metadata.
                                    If the user accept, send the information to the database using the tool "send_to_database".
                        """
        #Trocar o 4 por uma tool talvez

        qa_prompt = ChatPromptTemplate.from_messages(
            [
                ("system", qa_system_prompt),
                MessagesPlaceholder("chat_history"),
                ("human", "{input}"),
            ]
        )
        
        qa_chain = LLMChain(llm=Llm().openai(), prompt=qa_prompt)

        conversational_rag_chain = RunnableWithMessageHistory(
            qa_chain,
            self.get_by_session_id,
            input_messages_key="input",
            history_messages_key="chat_history",
            output_messages_key="output",
        )

        return conversational_rag_chain