# Gaia-X 7th Hackathon at the Tech-X Conference 2024.

This event has passed. Please find [the report](https://gaia-x.eu/wp-content/uploads/2024/05/Hackathon_Report.pdf) on the Gaia-X website

Please see informations directly on the [wiki](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/tech-x-2024-and-hackathon-7/-/wikis/home)


Please fork this repository and publish in it your progress, your documentation and whatever you want to share with the community and the judges